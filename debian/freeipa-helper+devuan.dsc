Format: 3.0 (quilt)
Source: freeipa-helper
Binary: freeipa-helper
Architecture: all
Version: 0.0.2-1+devuan
Maintainer: Ben Stack <bgstack15@gmail.com>
Homepage: https://gitlab.com/bgstack15/freeipa-helper
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~)
Package-List:
 freeipa-helper deb net optional arch=all
Files:
 00000000000000000000000000000000 1 freeipa-helper.orig.tar.gz
 00000000000000000000000000000000 1 freeipa-helper+devuan.debian.tar.xz
