# README for freeipa-helper
## Introduction
This is a set of scripts that facilitates installing FreeIPA client on systems that are not 100% Red Hat-like. The biggest issue is the lack of systemctl, which is hardcoded into the freeipa commands. After 5 minutes of starting to hack that codebase, I gave up. I'll lie to it instead.

## How to use
Install freeipa-helper package in addition to the regular freeipa-client, which takes some [special work](https://bgstack15.wordpress.com/2019/10/07/devuan-join-freeipa-domain/). See heading "Related packages" for additional requirements.

## More details
Using a simple script that captures all invocations of the affected commands, here is what the author captured:

    /bin/systemctl stop certmonger.service
    /bin/systemctl disable certmonger.service
    /bin/systemctl stop oddjobd.service
    /bin/systemctl disable oddjobd.service
    /bin/systemctl stop sssd.service
    /bin/systemctl disable sssd.service
    /usr/bin/hostnamectl set-hostname d2-04a
    /bin/systemctl list-unit-files --full
    /bin/systemctl list-unit-files --full
    /bin/systemctl stop chronyd.service
    /bin/systemctl disable chronyd.service
    /usr/bin/hostnamectl set-hostname d2-04a.ipa.smith122.com
    /bin/systemctl is-enabled chronyd.service
    /bin/systemctl enable chronyd.service
    /bin/systemctl restart chronyd.service
    /bin/systemctl is-active chronyd.service
    /usr/bin/systemd-detect-virt --container
    /usr/bin/systemd-detect-virt --container
    /bin/systemctl try-restart certmonger.service
    /bin/systemctl is-active certmonger.service
    /bin/systemctl list-unit-files --full
    /bin/systemctl list-unit-files --full
    /bin/systemctl is-active oddjobd.service
    /bin/systemctl is-enabled oddjobd.service
    /bin/systemctl restart sssd.service
    /bin/systemctl is-active sssd.service
    /bin/systemctl enable sssd.service
    /bin/systemctl is-active sshd.service
    /bin/systemctl restart sshd.service
    /bin/systemctl is-active sshd.service

So most of this is easily [translatable](https://gitlab.com/bgstack15/systemdtl) to real service commands. Unfortunately, chrony on devuan is flaky and never returns from an `update-rc.d chrony enable` so I gave up on that too.

Instead, we are going to just exit 0 on every invocation, while logging.

    {
       LOGFILE=/var/log/systemctl.log
       lecho "${0} ${*}" >> "${LOGFILE}"
    } 2>/dev/null
    :

## Alternatives
Use a distro with systemd which is almost a requirement for installing freeipa.
[https://gitlab.com/bgstack15/systemdtl](https://gitlab.com/bgstack15/systemdtl) is another project that attempts to satisfy ipa-client-install.

## License
[CC-BY-SA 4.0](https://choosealicense.com/licenses/cc-by-sa-4.0/)

## Description
Some scripts that sit on the filesystem exactly where ipa-client-install wants to find systemd-related commands. Use at your own risk!

## Upsides
Facilitates joining Devuan systems to a FreeIPA domain.

## Downsides
Has the potential to cause errors because the commands do not ever return failure. They just say "yes" to ipa-client-install, basically, which is probably not what it wants to hear every single time.

## Dependencies
Merely for the logging output: bgscripts [source](https://gitlab.com/bgstack15/bgscripts/) [deb](https://build.opensuse.org/package/show/home:bgstack15/bgscripts) [rpm](https://copr.fedorainfracloud.org/coprs/bgstack15/stackrpms/package/bgscripts/)

## Related packages
[oddjob-mkhomedir](https://build.opensuse.org/package/show/home:bgstack15/oddjob-mkhomedir) is required for freeipa-client on Devuan.
